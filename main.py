from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, WebAppInfo

# Инициализация бота
bot = Bot(token='')
dp = Dispatcher(bot)

@dp.message_handler(content_types=[types.ContentType.VIDEO])
async def process_video(message: types.Message):
    # Получите информацию о видео
    video = message.video
    await bot.send_message(message.from_user.id,text = str(video))
    # Отправьте видео обратно пользователю
    await bot.send_video(message.chat.id, video.file_id)

# Запуск бота
if __name__ == '__main__':
    executor.start_polling(dp)